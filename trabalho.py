# Define função que irá mostrar o menu inicial do programa e retornara o resultado escolhido pelo usuário
def menuPrincipal():
    print("Seja bem vindo!\n\nDigite a opção:\n\n\t1) Adicionar novo produto\n\t2) Remover produto\n\t3) Consultar produto\n\t4) listar todos produtos cadastrados\n\t5) Outra opcao\n\t6) Sair\n")  
    saida = input("-->")

    if saida.isdigit(): # Se a opcao digitada não corresponder a um numero, mas a uma palavra ou letra, o programa retornará um valor 
        return saida    # Que Não Possui uma opção correspondente
    else:               # Do Contrário, retornará a própria opção
        return 7        

# Função responsável por adicionar os itens na matriz "estoque", passada por referencia
def menuAdicionarItem(estoque):
    print("Adicionar novo produto")
    produto = input("Digite o nome do produto: ") # Captura as informações a respeito do alimento

    for linha in estoque:
        if produto == linha[0]:
            return 3

    preco = (input("Digite quanto o produto custará: "))

    if not preco.isalpha():
        preco = float(preco)
    else:
        return 1 # Retorna a função com código de erro

    quant = input("Digite quanto tem disponível do produto: ")

    if quant.isdigit():
        quant = int(quant)

    else:
        return 2
    
    estoque.append([produto, preco, quant]) # Adiciona uma nova linha à matriz

    return 0 # Retorna a função com código de sucesso

# Define função responsável por remover itens do estoque
def menuRemoverItem(estoque):
    produto_a_remover = input("Digite o nome do produto que você deseja remover: ") # Captura nome do produto a ser removido

    for linha in estoque: # Laço for que pega a matriz estoque e divide em linhas
        if linha[0] == produto_a_remover: # Testa para ver se a primeira coluna é igual ao nome do produto a ser removido
            estoque.remove(linha)
            return 0
        
# Define a função responsável por consultar um produto no estoque
def consultaProduto(estoque):
    produto = input("Digite o nome do produto que deseja consultar: ")
    counter = 0

    for linha in estoque:
        if linha[0] == produto:
            print("Nome do produto: ", linha[0], "\nPreço do produto: ", linha[1], "\nQuantidade disponível: ", linha[2] )

# Define função responsável por listar todos os produtos disponíveis
def listaEstoque(estoque): # Recebe por parametro a matriz do estoque
    for linha in estoque: # Divide-a em linhas
        print("Nome: ", linha[0], "\t| Preço: ", linha[1], "\t| Quantidade disponível: ", linha[2]) # Apresenta As Colunas de maneira organizada

#define função responsável por apresentar algumas estatísicas a respeito do estoque
def estatistica(estoque):

    # 1°- produto mais caro e produto mais barato

    produtos = []

    for linha in estoque:   
        produtos.append([linha[1], linha[0]]) # Cria uma nova matriz, organizando os itens primeiro pelo preço, depois pelo nome

    produtos = sorted(produtos) # Organiza os produtos

    print("Preço do produto mais barato", produtos[0][0], "\t |  Produto mais barato: ", produtos[0][1], "\nPreço do produto mais caro: ", produtos[len(estoque) - 1][0],"\t| Nome do produto mais caro: ", produtos[len(estoque) - 1][1])
    
    produtos.clear()

    # 2°- produto em menor e maior quantidade

    for linha in estoque:
        produtos.append([linha[2], linha[0]]) # Cria uma nova matriz, organizando os itens primeiro pela quantidade, depois pelo nome

    produtos = sorted(produtos) # Organiza os produtos

    print("Produto em menor quantidade", produtos[0][1], "\t |  Quantidade: ", produtos[0][0], "\nProduto em menor quantidade: ", produtos[len(estoque) - 1][1],"\t| Quantidade: ", produtos[len(estoque) - 1][0])

    # 3°- Média da quantidade de itens por tipo de produto

    qtd_total = 0 # Quantidade total de produtos disponíveis
    itens_cadastrados = len(estoque) # Quantidade de itens cadastrados

    for linha in estoque:
        qtd_total += linha[2] # Incrementa a quantidade de acordo com a quantidade de certo item no estoque

    print("\nMédia de quantidade por produto: ", qtd_total/itens_cadastrados)


    

# Função main
def main():

    estoque = [["Arroz", 7.95, 15], ["Feijao", 6.95, 28], ["Linguiça", 3.40, 150]]

    while(1==1):
        print("\n\n\n\n")
        escolha = int(menuPrincipal()) # chama o menu principal. Faz O cast para int para poder ser feita a comparação logo abaixo
    
        if(escolha == 1):
            
            op = menuAdicionarItem(estoque)
            
            if(op == 1):
                print("\nErro! O preço precisa ser um numero!\n")
            elif(op == 2):
                print("\nErro! A quantidade precisa ser um numero inteiro\n")
            elif(op == 3):
                print("\nErro! Produto já cadastrado\n")
            else:
                print("\nsucesso! Produto Cadastrado Com sucesso\n")

        elif(escolha == 2):

            op = menuRemoverItem(estoque)

            if not(op == 0):
                print("\nErro! Produto Não encontrado\n")
            else:
                print("\nSucesso! Produto Removido com sucesso\n")


        elif(escolha == 3):

            consultaProduto(estoque)

        elif(escolha == 4):

            listaEstoque(estoque)

        elif(escolha == 5):

            estatistica(estoque)

        elif(escolha == 6):

            exit(0)

        else:

            print("Opcao inválida, tente novamente!")

main()
